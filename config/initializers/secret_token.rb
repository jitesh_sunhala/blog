# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBlog::Application.config.secret_key_base = '64cfe54557834c3bcc789cdf837ded69063f7091ab63a58aabe4125e294c0e6c9d7fe5e0e56d11add8f91059cf8dac7a090980ac7b2887c36dd8884f1d0d0906'
